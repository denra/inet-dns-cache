# Caching DNS-server
---

Version 1.0

Author: Razbitsky Denis ([idenra@gmail.com](idenra@gmail.com))

---

## Description
---

This application is the implementation of [caching dns-server](https://en.wikipedia.org/wiki/Name_server#Caching_name_server).

Also it is the solution of inet course task named [dns-cache](http://anytask.urgu.org/course/51).

---

## Requirements
---

* Python >= 3.4

---

## The work scheme